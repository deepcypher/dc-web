# @Author: GeorgeRaven <archer>
# @Date:   2020-09-16T12:08:37+01:00
# @Last modified by:   archer
# @Last modified time: 2020-09-16T12:11:52+01:00
# @License: please see LICENSE file in project root
import flask_restful  # for api
from flask_restful import reqparse  # for argument parsing
from .users import is_authenticated
from fhe.reseal import Reseal, ReScheme
import marshmallow


class Decrypt(flask_restful.Resource):

    def put(self):
        argparser = reqparse.RequestParser()
        argparser.add_argument("pass", type=str, required=True,
                               help="password for authentication required")
        argparser.add_argument("name", type=str, required=True,
                               help="username for authentication required")
        argparser.add_argument("data", required=True,
                               help="List of ReSeal data required to store.")
        args = argparser.parse_args()

        # validate and sort out user data
        scheme = ReScheme()  # scheme to validate against
        args["data"] = scheme.loads(args["data"])  # load and validate

        authenticated = is_authenticated(args["name"], args["pass"])
        if authenticated:
            r = Reseal()
            r.__setstate__(args["data"])
            plaintext = r.plaintext
            dict_schema = {
                "data": marshmallow.fields.List(marshmallow.fields.Float()),
            }
            response = marshmallow.Schema.from_dict(
                dict_schema)().dumps(plaintext)
            return response
        else:
            return {"message":
                    "error, user '{}' failed authentication".format(
                        args["name"])}, 403
