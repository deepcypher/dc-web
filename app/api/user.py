# @Author: GeorgeRaven <archer>
# @Date:   2020-08-26T12:44:08+01:00
# @Last modified by:   archer
# @Last modified time: 2020-09-14T15:20:38+01:00
# @License: please see LICENSE file in project root


from ezdb.mongo import Mongo
from api.api import arg_handler
from passlib.hash import argon2
import bson
import logging


class User():
    """Flask-login compatible user class for user authentication."""

    def __init__(self, password=None, username=None, id=None):

        logging.debug("USER: username={}, password={}, id={}".format(
            username, password, id))
        self._username = username
        self._password = password
        self._id = id

        if username or id:
            pipeline = [
                {"$match": {"_id": bson.objectid.ObjectId(id)} if id is not
                 None else {"username": username}
                 }
            ]
            logging.debug("USER: pipeline; {}".format(pipeline))
            docs = list(self._finds(pipeline=pipeline, collection="users"))
            if len(docs) == 0:
                self._doc = None
            elif len(docs) == 1:
                self._doc = docs[0]
            else:
                raise ValueError("Too many users of the same name: {}".format(
                    username if username is not None else id
                ))
        else:
            raise ValueError("Neither username nor id has been defined.")

    def _finds(self, pipeline, collection):
        """Find data from db."""
        db = Mongo(arg_handler())
        db.connect()
        logging.debug("db")
        cursor = db.getCursor(db_collection_name=collection,
                              db_pipeline=pipeline)
        logging.debug("cursor")
        for batch in db.getBatches(db_data_cursor=cursor):
            for doc in batch:
                yield doc

    @property
    def doc(self):
        return self._doc

    @property
    def name(self):
        return self.doc["username"]

    @property
    def hasher(self):
        if self.__dict__.get("_hasher"):
            return self._hasher
        else:
            self._hasher = argon2.using(type="ID")
            return self.hasher

    @property
    def is_authenticated(self):
        """Check if a user has valid credentials.
        This property should return True if the user is authenticated, i.e.
        they have provided valid credentials. (Only authenticated users will
        fulfill the criteria of login_required.)"""
        if self.doc and self._password:
            hasher = self.hasher
            result = hasher.verify(self._password, self.doc["hash"])
        elif self.doc:
            # I dont like this, I really dont. wtf flask-login!
            # you are telling me if someone mereley has their ID and spoofs
            # the cookie that they can authenticate as anyone?
            # pureley because flask login only stores some form of ID
            # even if loading from cookie means the session is stale I dont
            # like this at all!
            result = True
        else:
            result = False
        logging.debug("USER: is_authenticated? {}".format(result))
        return result

    @property
    def is_active(self):
        """Check if this user has an active account.
        This property should return True if this is an active user -
        in addition to being authenticated, they also have activated their
        account, not been suspended, or any condition your application has for
        rejecting an account. Inactive accounts may not log in (without being
        forced of course)."""
        if self.doc:
            # if active field exists in user doc, else assume is normal active
            if self.doc.get("active"):
                result = self.doc["active"] is True
            else:
                result = True
        else:
            result = False
        logging.debug("USER: is_active? {}".format(result))
        return result

    @property
    def is_anonymous(self):
        """Check if user is some form of Anon.
        This property should return True if this is an anonymous user.
        (Actual users should return False instead.)"""
        result = False
        logging.debug("USER: is_anonymous? {}".format(result))
        return result

    def get_id(self):
        """Get user id from db.
        This method must return a unicode that uniquely identifies this
        user, and can be used to load the user from the user_loader callback.
        Note that this must be a unicode - if the ID is natively an int or some
        other type, you will need to convert it to unicode."""
        if self.doc:
            result = str(self.doc["_id"])
        else:
            result = None
        logging.debug("USER: get_id? {}".format(result))
        return result
