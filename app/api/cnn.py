# @Author: GeorgeRaven <archer>
# @Date:   2020-09-16T11:33:51+01:00
# @Last modified by:   archer
# @Last modified time: 2020-09-18T13:34:04+01:00
# @License: please see LICENSE file in project root

import logging as logger
import numpy as np
from fhe.reseal import Reseal


class Layer_CNN():

    def __init__(self, weights, bias):
        self.cc = Cross_Correlation()
        self.cc.weights = weights
        self.cc.bias = bias

    @property
    def activation_function(self):
        if self.__dict__.get("_activation_function") is not None:
            return self._activation_function
        else:
            self.activation_function = Sigmoid_Approximation()
            return self.activation_function

    @activation_function.setter
    def activation_function(self, activation_function):
        self._activation_function = activation_function

    def forward(self, x):
        """Take batch of x and return activated output of this layer."""
        cross_correlated = self.cc.forward(x)
        activated = self.activation_function.forward(cross_correlated)
        return activated

    def backward(self, gradient):
        local_gradient = self.cc.backward()
        return local_gradient

    def update(self):
        self.cc.update()


class Sigmoid_Approximation():

    def __init__(self):
        self._cache = {}

    @property
    def x_plain(self):
        """Plaintext x for backward pass"""
        return self._cache["x"]

    @x_plain.setter
    def x_plain(self, x):
        if isinstance(x, Reseal):
            self._cache["x"] = x.plaintext
        else:
            self._cache["x"] = x

    def forward(self, x):
        self.x_plain = x
        return 0.5 + (0.197 * x) + ((-0.004 * x) * (x * x))

    def backward(self, gradient):
        # calculate local gradient but using normal sigmoid derivative
        # as this is approximate and is faster this way
        # \frac{d\sigma}{dx} = (1-\sigma(x))\sigma(x)
        x = self.x_plain  # get our cached input to calculate gradient
        local_gradient = (1 - self.sigmoid(x)) * self.sigmoid(x) * gradient
        return local_gradient

    def update(self):
        # new_parameter = old_parameter - learning_rate * gradient_of_parameter
        raise NotImplementedError(
            "Sigmoid approximation has no parameters to update")

    def sigmoid(self, x):
        return (1/(1+np.exp(-x)))


class Cross_Correlation():

    def __init__(self):
        self._cache = {}

    @property
    def weights(self):
        return self._weights

    @weights.setter
    def weights(self, weights):
        # initialise weights from tuple dimensions
        # TODO: properly implement xavier weight initialisation over np.rand
        if isinstance(weights, tuple):
            # https://www.coursera.org/specializations/deep-learning
            # https://towardsdatascience.com/weight-initialization-techniques-in-neural-networks-26c649eb3b78
            self._weights = np.random.rand(*weights)
        else:
            self._weights = weights

    @property
    def bias(self):
        logger.debug("CNN BIAS CONDITION: {}".format(self.__dict__.get(
            "_bias" is None)))
        if self.__dict__.get("_bias") is not None:
            return self._bias
        else:
            self.bias = 0
            logger.debug("CNN bias: {}".format(self.bias))
            return self.bias

    @bias.setter
    def bias(self, bias):
        self._bias = bias
        logger.debug("CNN BIAS; bias now: {}".format(self._bias))

    @property
    def x_plain(self):
        """Plaintext x for backward pass"""
        return self._cache["x"]

    @x_plain.setter
    def x_plain(self, x):
        if isinstance(x, Reseal):
            self._cahce["x"] = x.plaintext
        else:
            self._cache["x"] = x

    def forward(self, x):
        logger.debug("CNN forward, batch: {}, range: {}".format(
            x, range(len(x))))
        sum = None
        for t in range(len(x)):
            logger.debug("CNN row original: {}".format(x[t].plaintext))
            logger.debug("CNN row weights: {}".format(self.weights[0][t][0]))
            convolution = x[t] * self.weights[0][t][0]
            logger.debug("CNN row convolution: {}".format(
                convolution.plaintext))
            if sum is None:
                sum = convolution
            else:
                sum = sum + convolution
        logger.debug("CNN sum: {}, bias: {}".format(sum, self.bias))
        biased = sum + self.bias
        return biased  # return the now biased convolution ready for activation

    def backward(self, gradient):
        # calculate local gradient
        x = self.x_plain  # plaintext of x for backprop
        # df/dbias is easy as its addition so its same as previous gradient
        self.bias_gradient = gradient * 1  # uneccessary but here for clarity
        # df/dweights is also simple as it is a chain of addition with a single
        # multiplication against the input so the derivative is just gradient
        # multiplied by input
        self.weights_gradients = x * gradient
        local_gradient = 0  # dont care as end of computational chain
        return local_gradient

    def update(self, learning_rate=None):
        """We need to update 2 things, both the biases and the weights"""
        learning_rate = learning_rate if learning_rate is not None else 0.001
        # new_parameter = old_parameter - learning_rate * gradient_of_parameter
        self.bias = self.bias - (learning_rate * self.bias_gradient)
        self.weights = self.weights - (learning_rate * self.weights_gradients)
