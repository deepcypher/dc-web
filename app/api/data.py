# @Author: GeorgeRaven <archer>
# @Date:   2020-09-16T12:06:21+01:00
# @Last modified by:   archer
# @Last modified time: 2020-09-16T12:11:05+01:00
# @License: please see LICENSE file in project root
import flask_restful  # for api
from flask_restful import reqparse  # for argument parsing
from ezdb.mongo import Mongo  # mongodb handler
from .users import is_authenticated, get_user
from .args import arg_handler
import datetime
import logging as logger
from fhe.reseal import ReScheme
import bson


class Data(flask_restful.Resource):
    def get(self):
        argparser = reqparse.RequestParser()
        argparser.add_argument("pass", type=str, required=True,
                               help="password for authentication required")
        argparser.add_argument("name", type=str, required=True,
                               help="username for authentication required")
        argparser.add_argument("id", type=str, required=True,
                               help="id to retrieve data required.")
        args = argparser.parse_args()

        logger.debug("DATA GET")
        authenticated = is_authenticated(args["name"], args["pass"])
        if authenticated:
            user = get_user(args)
            db = Mongo(arg_handler())
            pipeline = [
                {'$match': {'owner': bson.objectid.ObjectId(user["id"])}},
                {'$match': {'_id': bson.objectid.ObjectId(args["id"])}}
            ]
            db.connect()
            cursor = db.getCursor(db_collection_name="data",
                                  db_pipeline=pipeline)
            for batch in db.getBatches(db_data_cursor=cursor):
                for item in batch:
                    reseal_dict = item["data"]
            ReScheme().validate(reseal_dict)
            return {"id": str(item["_id"]),
                    "data": ReScheme().dump(reseal_dict)}
        else:
            return {"message",
                    "ERROR, user '{}' failed authentication".format(
                        args["name"])}, 403

    def put(self):
        logger.debug("DATA PUT start")

        argparser = reqparse.RequestParser()
        argparser.add_argument("pass", type=str, required=True,
                               help="password for authentication required")
        argparser.add_argument("name", type=str, required=True,
                               help="username for authentication required")
        argparser.add_argument("data", required=True,
                               help="List of ReSeal data required to store.")
        argparser.add_argument("set", default=None,
                               help="data set/ name to associate")
        args = argparser.parse_args()

        # validate and sort out user data
        scheme = ReScheme()  # scheme to validate against
        args["data"] = scheme.loads(args["data"])  # load and validate

        logger.debug("DATA PUT: poly_mod = {}".format(
            args["data"]["_poly_modulus_degree"]))
        authenticated = is_authenticated(args["name"], args["pass"])
        if authenticated:
            logger.debug("DATA PUT authenticated")
            user = get_user(args)
            db = Mongo(arg_handler())
            db.connect()
            document = {
                "data": args["data"],
                "set": args["set"],
                "owner": bson.objectid.ObjectId(user["id"]),
                "datetime": datetime.datetime.utcnow()
            }
            id = db.dump(db_collection_name="data", data=document)
            logger.debug("DATA PUT data dumped: _id({})".format(str(id)))
        else:
            return {"message",
                    "error, user '{}' failed authentication".format(
                        args["name"])}, 403
        logger.debug("DATA PUT end")
        return {"id": str(id.inserted_id)}
