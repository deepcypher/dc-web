# @Author: GeorgeRaven <archer>
# @Date:   2020-08-29T20:42:43+01:00
# @Last modified by:   archer
# @Last modified time: 2020-09-16T12:16:41+01:00
# @License: please see LICENSE file in project root


import flask
import flask_restful  # for api
from .args import arg_handler
from .data import Data
from .users import Users
from .user_data import User_Data
from .infer import Infer
from .train import Train
from .encrypt import Encrypt
from .decrypt import Decrypt


def api_main():
    api_bp = flask.Blueprint(
        "api", __name__,
        template_folder="templates", static_folder="static")

    api = flask_restful.Api(api_bp)

    api.add_resource(Data, "/data")
    api.add_resource(Users, "/user")
    api.add_resource(User_Data, "/user/data")
    api.add_resource(Infer, "/infer")
    api.add_resource(Train, "/train")
    api.add_resource(Encrypt, "/encrypt")
    api.add_resource(Decrypt, "/decrypt")
    api.add_resource(Info, "/info")

    return api_bp


class Info(flask_restful.Resource):

    def get(self):
        state = arg_handler()
        return {
            "version": state["version"]
        }


if __name__ == "__main__":
    app = flask.Flask(__name__)
    # generating API object
    api = api_main()
    # registering blueprint so we can test it if this file is run alone
    app.config["SERVER_NAME"] = "cryptolog.io"
    # app.config["SERVER_NAME"] = "localhost:8000"
    app.register_blueprint(api, subdomain="api")
    # print(app.config)
    # print(app.url_map)
    app.run(
        host="localhost",
        port=443,
        debug=True,
        ssl_context="adhoc"  # gaff https
    )
