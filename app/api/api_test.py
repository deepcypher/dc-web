#!/usr/bin/env python3

# @Author: GeorgeRaven <archer>
# @Date:   2020-08-18T11:33:45+01:00
# @Last modified by:   archer
# @Last modified time: 2021-01-13T16:45:57+00:00
# @License: please see LICENSE file in project root


from fhe.reseal import Reseal, ReScheme
import time
import marshmallow
import requests
import unittest
import numpy as np
import string
import random
# disabling the plethora of warning messages that can be shot out in testing
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class WebTest(unittest.TestCase):

    def setUp(self):
        self.startTime = time.time()

    def tearDown(self):
        t = time.time() - self.startTime
        print('%s: %.3f' % (self.id(), t))

    @property
    def url(self):
        url = "https://api.deepcypher.me"
        return url

    @property
    def uri_data(self):
        uri = "data"
        return uri

    @property
    def uri_user(self):
        uri = "user"
        return uri

    @property
    def uri_user_data(self):
        uri = "user/data"
        return uri

    @property
    def uri_train(self):
        uri = "train"
        return uri

    @property
    def uri_infer(self):
        uri = "infer"
        return uri

    @property
    def uri_encrypt(self):
        uri = "encrypt"
        return uri

    @property
    def uri_decrypt(self):
        uri = "decrypt"
        return uri

    @property
    def uri_info(self):
        uri = "info"
        return uri

    @property
    def randomness_length(self):
        return 32

    @property
    def random_name(self):
        letters_and_digits = string.ascii_letters + string.digits
        result_str = ''.join((random.choice(letters_and_digits) for i in range(
            self.randomness_length)))
        return result_str

    @property
    def reseal_defaults(self):
        # https://pynative.com/python-generate-random-string/
        defaults = {
            "scheme": 2,
            "poly_modulus_degree": 8192,
            "coefficient_modulus": [60, 40, 40, 60],
            "scale": pow(2.0, 40),
            "cache": True,
        }
        return defaults

    def test_resource_data_put(self):
        set_name = self.random_name
        r = Reseal(**self.reseal_defaults)
        r.ciphertext = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
        reseal_dict = r.__getstate__()
        data = {"pass": "password1234",
                "name": "groot",
                "data": ReScheme().dumps(reseal_dict),
                "set": set_name  # setting the collection of data a name
                }

        id_ = self.data_put(put_data=data)
        self.assertTrue(id_)

    def data_put(self, put_data):
        response = requests.put(
            "{}/{}".format(self.url, self.uri_data),
            data=put_data,
            verify=False)
        if not response:
            print("DATA PUT: {}".format(response.content))
            raise ValueError("Response failed")
            return None
        return response.json().get("id")

    def test_resource_data_get(self):
        data = {
            "name": "groot",
            "pass": "password1234",
        }
        response = requests.get(
            "{}/{}".format(self.url, self.uri_user_data),
            data=data,
            verify=False)
        if not response:
            print(response.content)
        self.assertTrue(response)

        # now use response to get most recent id to request for
        data = {"pass": "password1234",
                "name": "groot",
                "id": response.json()["data"][0]["id"]}
        response = requests.get(
            "{}/{}".format(self.url, self.uri_data),
            data=data,
            verify=False)
        if not response:
            print(response.content)
        self.assertTrue(response)
        returned_dict = response.json()
        self.assertTrue(returned_dict.get("id"))
        returned_data = ReScheme().load(returned_dict["data"])
        r = Reseal()
        r.__setstate__(returned_data)

    def test_resource_user_put(self):
        data = {"pass": "password1234",
                "name": "groot",
                "new_pass": "ILove2Encrypt",
                "new_name": "ioft"}
        response = requests.put(
            "{}/{}".format(self.url, self.uri_user),
            data=data,
            verify=False)
        if not response:
            print(response.content)
        # if user already exists we will get an error as expected
        if response.status_code == 409:
            pass
        else:
            self.assertTrue(response)
            self.assertTrue(response.json().get("id"))
            # self.assertEqual(response.json(), data)

    def test_resource_user_get(self):
        data = {"name": "groot"}
        response = requests.get(
            "{}/{}".format(self.url, self.uri_user),
            data=data,
            verify=False)
        if not response:
            print(response.content)
        self.assertTrue(response)
        self.assertEqual(response.json()["username"], data["name"])
        print(response.json())

    def test_resource_user_data_get(self):
        data = {
            "name": "groot",
            "pass": "password1234",
        }
        response = requests.get(
            "{}/{}".format(self.url, self.uri_user_data),
            data=data,
            verify=False)
        if not response:
            print(response.content)
        self.assertTrue(response)
        # of the return document, get the data ids, in position 0
        # (the most recent)
        response.json()["data"][0]["id"]

    def test_resource_train(self):
        # is reseal avaliable locally
        is_local = True
        # setting encryption parameters
        params = {
            "scheme": 2,
            "poly_modulus_degree": 8192*2,
            "coefficient_modulus": [60, 40, 40, 40, 60],
            "scale": pow(2.0, 40),
            "cache": True,
        }
        # encrypting desired data to compute
        input = np.array([[0.1, 0.2, 0.3, 0.4],
                          [0.5, 0.6, 0.7, 0.8],
                          [0.9, 1.0, 0.1, 0.2]])
        if is_local:
            r = Reseal(**params)
            r.ciphertext = [0]  # initialising so we can pinch copies
        encrypted_list = None
        for i in input:  # TODO make this share keys in this batch
            if is_local:  # when localy avaliable use reseal
                i_r = r.duplicate()  # this ensures they share the same keys
                i_r.ciphertext = i
                # print("TEST RESOURCE INFER:", i_r)
                i_r = i_r.__getstate__()  # getstate ready for transmission
            else:  # when not local use remote encryptor
                i_r = self.remote_encrypt(put_data={
                    "name": "groot",
                    "pass": "password1234",
                    "data": i,
                })
            if encrypted_list == None:
                encrypted_list = [i_r]
            else:
                encrypted_list.append(i_r)
        # sending desired data to server so it can be used for inference
        set_name = self.random_name  # name shared between rows as an alt id
        ids = []
        for i in encrypted_list:
            request_dictionary = {
                "name": "groot",
                "pass": "password1234",
                "data": ReScheme().dumps(i),
                "set": set_name
            }
            id_ = self.data_put(put_data=request_dictionary)
            ids.append(id_)
        print("INFER ids: {}".format(ids))
        # telling the API which data to use for deep learning batch
        data = {
            "name": "groot",
            "pass": "password1234",
            "set": set_name,
            # "ids": ids,
            "sequence_length": 3,
        }
        response = requests.put(
            "{}/{}".format(self.url, self.uri_train),
            data=data,
            verify=False)
        if not response:
            print(response.content)
        self.assertTrue(response)

    def test_resource_infer(self):
        # is reseal avaliable locally
        is_local = True
        # setting encryption parameters
        params = {
            "scheme": 2,
            "poly_modulus_degree": 8192*2,
            "coefficient_modulus": [60, 40, 40, 40, 60],
            "scale": pow(2.0, 40),
            "cache": True,
        }
        # encrypting desired data to compute
        input = np.array([[0.1, 0.2, 0.3, 0.4],
                          [0.5, 0.6, 0.7, 0.8],
                          [0.9, 1.0, 0.1, 0.2]])
        if is_local:
            r = Reseal(**params)
            r.ciphertext = [0]  # initialising so we can pinch copies
        encrypted_list = None
        for i in input:  # TODO make this share keys in this batch
            if is_local:  # when localy avaliable use reseal
                i_r = r.duplicate()  # this ensures they share the same keys
                i_r.ciphertext = i
                # print("TEST RESOURCE INFER:", i_r)
                i_r = i_r.__getstate__()  # getstate ready for transmission
            else:  # when not local use remote encryptor
                i_r = self.remote_encrypt(put_data={
                    "name": "groot",
                    "pass": "password1234",
                    "data": i,
                })
            if encrypted_list == None:
                encrypted_list = [i_r]
            else:
                encrypted_list.append(i_r)
        # sending desired data to server so it can be used for inference
        set_name = self.random_name  # name shared between rows as an alt id
        ids = []
        for i in encrypted_list:
            request_dictionary = {
                "name": "groot",
                "pass": "password1234",
                "data": ReScheme().dumps(i),
                "set": set_name
            }
            id_ = self.data_put(put_data=request_dictionary)
            ids.append(id_)
        print("INFER ids: {}".format(ids))
        # telling the API which data to use for deep learning batch
        data = {
            "name": "groot",
            "pass": "password1234",
            "set": set_name,
            # "ids": ids,
            "sequence_length": 3,
        }
        response = requests.put(
            "{}/{}".format(self.url, self.uri_infer),
            data=data,
            verify=False)
        if not response:
            print(response.content)
        self.assertTrue(response)

    def test_resource_encrypt(self):
        data = {
            "name": "groot",
            "pass": "password1234",
            "data": np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        }
        output_data = self.remote_encrypt(put_data=data)
        r = Reseal()
        r.__setstate__(output_data)
        decrypted = r.plaintext
        # print("Before encryption: {}\nDecrypted again: {}".format(
        #     data["data"],
        #     decrypted))

    def remote_encrypt(self, put_data):
        dict_schema = {
            "name": marshmallow.fields.Str(),
            "pass": marshmallow.fields.Str(),
            "data": marshmallow.fields.List(marshmallow.fields.Float()),
            "options": marshmallow.fields.Dict()
        }
        schema = marshmallow.Schema.from_dict(dict_schema)()
        data_dump = schema.dumps(put_data)
        response = requests.put(
            "{}/{}".format(self.url, self.uri_encrypt),
            data=data_dump,
            verify=False)
        if not response:
            print(response.content)
            return None
        else:
            reseal_state = ReScheme().loads(response.json()["data"])
            return reseal_state

    def test_resource_decrypt(self):
        # encrypt first
        dict_schema = {
            "name": marshmallow.fields.Str(),
            "pass": marshmallow.fields.Str(),
            "data": marshmallow.fields.List(marshmallow.fields.Float())
        }
        data = {
            "name": "groot",
            "pass": "password1234",
            "data": np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        }
        schema = marshmallow.Schema.from_dict(dict_schema)()
        data_dump = schema.dumps(data)
        response = requests.put(
            "{}/{}".format(self.url, self.uri_encrypt),
            data=data_dump,
            verify=False)
        if not response:
            print(response.content)
        self.assertTrue(response)

        # decrypt second
        output_data = ReScheme().loads(response.json()["data"])
        r = Reseal()
        r.__setstate__(output_data)
        decrypted = r.plaintext

        decrypt_by_server = r.__getstate__()
        data = {"pass": "password1234",
                "name": "groot",
                "data": ReScheme().dumps(decrypt_by_server)}
        response = requests.put(
            "{}/{}".format(self.url, self.uri_decrypt),
            data=data,
            verify=False)
        if not response:
            print(response.content)
        self.assertTrue(response)
        dict_schema = {
            "data": marshmallow.fields.List(marshmallow.fields.Float()),
        }
        server_decrypted = marshmallow.Schema.from_dict(dict_schema)().loads(
            response.json())
        self.assertEqual(decrypted.tolist(), server_decrypted["data"])

    def test_resource_info(self):
        response = requests.get(
            "{}/{}".format(self.url, self.uri_info),
            verify=False)
        if not response:
            print(response.content)
        self.assertTrue(response)
        print("VERSION: {}".format(response.json()["version"]))


if __name__ == "__main__":
    # run all the unit-tests
    print("now testing:", __file__, "...")
    unittest.main()
