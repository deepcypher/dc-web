# @Author: GeorgeRaven <archer>
# @Date:   2020-09-16T11:53:50+01:00
# @Last modified by:   archer
# @Last modified time: 2020-09-16T12:02:02+01:00
# @License: please see LICENSE file in project root

import flask_restful  # for api
from flask_restful import reqparse  # for argument parsing
import bson
from ezdb.mongo import Mongo
import logging as logger
from .auth import Auth  # authentication utility
import datetime
from .args import arg_handler


class Users(flask_restful.Resource):
    def get(self):
        argparser = reqparse.RequestParser()
        argparser.add_argument("name", type=str,
                               help="Name to look up id.")
        argparser.add_argument("id", type=str,
                               help="Id to look up name.")
        args = argparser.parse_args()
        return get_user(args)

        return {"message": "ERROR: no id or username provided to look up."}

    def put(self):
        argparser = reqparse.RequestParser()
        argparser.add_argument("pass", type=str, required=True,
                               help="password for authentication required")
        argparser.add_argument("name", type=str, required=True,
                               help="username for authentication required")
        argparser.add_argument("new_pass", type=str, required=True,
                               help="new password to assign required")
        argparser.add_argument("new_name", type=str, required=True,
                               help="new username to assign required")
        args = argparser.parse_args()
        password = args["pass"]
        username = args["name"]
        new_password = args["new_pass"]
        new_username = args["new_name"]
        role = "user"
        authenticated = is_authenticated(username, password)
        username_free = is_username_free(new_username)
        logger.debug("authenticated: {}, is free: {}".format(authenticated,
                                                             username_free))
        if authenticated and username_free:
            # create user if user does not already exist
            logger.debug("USER CREATION: \"{}\" creating \"{}\"".format(
                username, new_username))
            payload = {
                "username": new_username,
                "hash": Auth(new_password).hash,
                "role": role,
                "active": True,
                "datetime": datetime.datetime.utcnow(),
            }
            db = Mongo(arg_handler())
            db.connect()
            result = db.dump(db_collection_name="users", data=payload)
            output = {
                "id": str(result.inserted_id),
            }
            return output
        elif not authenticated:
            raise ValueError(
                "Username or password is incorrect/ too many of same name.")
        elif not username_free:
            # flask.abort()
            return {"message": "ERROR: user already exists."}, 409
            raise ValueError("Username is not free to create or change to")
        else:
            raise ValueError("Unexpected error")


def get_user(args):
    pipeline = None
    if args.get("name"):
        pipeline = [
            {"$match": {"username": args.get("name")}}
        ]
    elif args.get("id"):
        pipeline = [
            {"$match": {"_id": bson.objectid.ObjectId(args.get("id"))}}
        ]

    if pipeline is not None:
        # create database object
        db = Mongo(arg_handler())
        db.connect()
        cursor = db.getCursor(db_collection_name="users",
                              db_pipeline=pipeline)
        for batch in db.getBatches(db_data_cursor=cursor):
            for doc in batch:
                return {"id": str(doc["_id"]),
                        "username": doc["username"]}


def is_authenticated(username, password):
    # create database object
    db = Mongo(arg_handler())
    db.connect()

    # create user check pipeline and cursor
    pipeline = [
        {"$match": {"username": username}},
    ]
    cursor = db.getCursor(db_collection_name="users",
                          db_pipeline=pipeline)

    # use cursor to get user from database and verify password and
    # that they are the only user of this name
    for batch in db.getBatches(db_data_cursor=cursor):
        if len(batch) == 1:
            for doc in batch:
                if Auth(password=password, hash=doc["hash"]).verify():
                    logger.debug("PASSING VERIFICATION")
                    return True
        else:
            logger.error("too many users with name: {}".format(username))
            return False
    return False


def is_username_free(username):
    # create database object
    db = Mongo(arg_handler())
    db.connect()

    # create user check pipeline and cursor
    pipeline = [
        {"$match": {"username": username}},
    ]
    cursor = db.getCursor(db_collection_name="users",
                          db_pipeline=pipeline)

    # use cursor to get user from database and verify password and
    # that they are the only user of this name
    for batch in db.getBatches(db_data_cursor=cursor):
        return False
    return True
