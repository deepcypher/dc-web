#!/usr/bin/env python3

# @Author: GeorgeRaven <archer>
# @Date:   2020-08-30T13:55:54+01:00
# @Last modified by:   archer
# @Last modified time: 2020-10-20T22:32:28+01:00
# @License: please see LICENSE file in project root

import os
import sys
import flask
import flask_login
import lesscpy
import logging
import bson
from ezdb.mongo import Mongo
from urllib.parse import urlparse, urljoin
from api.args import arg_handler
from fhe.reseal import Reseal, ReScheme

import pandas as pd
import datetime

# allowing importing as if from parent directory so we can use api libs
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def is_safe_url(target):
    # https://stackoverflow.com/a/61446498/11164973
    ref_url = urlparse(flask.request.host_url)
    test_url = urlparse(urljoin(flask.request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
        ref_url.netloc == test_url.netloc


def web_main():
    from api.user import User
    # initialising blueprint
    web_bp = flask.Blueprint(
        "web",
        __name__,
        template_folder="templates",
        static_folder="static"
    )
    # purposefull cookie invalidation
    # web_bp.secret_key = os.urandom(256)
    # logging.debug("SECRET KEY: {}".format(web_bp.secret_key))

    # using flask-login to handle login/ session management
    login_manager = login_management(web_bp)

    # compiling less assets to CSS
    src_folder, _ = os.path.split(os.path.abspath(__file__))
    less_assets = {
        # key value pairs of paths or file-like objects to compile
        "{}/static/src/less/default.less".format(src_folder):
            "{}/static/css/default_bundle.css".format(src_folder),
    }
    build_less_assets(less_assets)

    # register login manager to app directly on blueprint load!
    @web_bp.record_once
    def on_load(state):
        login_manager.init_app(state.app)

    # error handlers

    # @web_bp.errorhandler(404) # use this instead for local errors only
    @web_bp.app_errorhandler(404)  # global 404 errors
    def page_no_found(e):
        return flask.render_template("404.jinja", state=arg_handler()), 404

    # web requests section

    @web_bp.route("/")
    def index():
        logging.debug("SESSION={}".format(flask.session))
        return flask.render_template("landing.jinja")

    @web_bp.route("/login", methods=["GET", "POST"])
    def login():
        logging.debug("SESSION={}".format(flask.session))
        if flask.request.method == "POST":
            usr = User(username=flask.request.form["username"],
                       password=flask.request.form["password"])
            if usr.is_authenticated:
                logging.debug("LOGIN user auth: {}, actv: {}".format(
                    usr.is_authenticated,
                    usr.is_active))
                flask_login.login_user(usr)
                # flask_login.confirm_login()
                next = flask.request.args.get("next")
                if not is_safe_url(next):
                    return flask.abort(400)

                logging.debug("NEXT: {}".format(next))
                return flask.redirect(next or flask.url_for(".profile"))
            else:
                return "Bad Login"
        else:
            return flask.render_template("login.jinja")

    @web_bp.route("/logout")
    def logout():
        logging.debug("SESSION={}".format(flask.session))
        flask_login.logout_user()
        return flask.redirect(flask.url_for(".index"))

    @web_bp.route("/profile")
    @flask_login.login_required
    def profile():
        logging.debug("SESSION={}".format(flask.session))
        return flask.render_template("profile.jinja")

    @web_bp.route("/info")
    def info():
        return flask.render_template("info.jinja", state=arg_handler())

    @web_bp.route("/base")
    def base():
        logging.debug("SESSION={}".format(flask.session))
        return flask.render_template("base.jinja")

    @web_bp.route("/help")
    def help():
        logging.debug("SESSION={}".format(flask.session))
        return flask.render_template("help.jinja")

    @web_bp.route("/docs")
    def docs():
        logging.debug("SESSION={}".format(flask.session))
        return flask.render_template("docs.jinja")

    @web_bp.route("/dashboard")
    @flask_login.login_required
    def dashboard():
        logging.debug("SESSION={}, user={}".format(
            flask.session, flask_login.current_user.get_id()))
        return flask.render_template("dashboard.jinja")

    @web_bp.route("/dashboard/upload", methods=["POST"])
    @flask_login.login_required
    def dashboard_upload():
        logging.debug("SESSION={}, user={}".format(
            flask.session, flask_login.current_user.get_id()))
        usr = flask_login.current_user
        set = flask.request.form.get("set")
        set = set if set is not "" else None
        # posting multiple files
        files = flask.request.files.getlist("file")
        logging.debug("/dashboard/upload set:{}, files:{}".format(set, files))
        for document in files:
            logging.debug("/dashboard/upload file:{}, file type: {}".format(
                document, type(document)))
            try:
                df = pd.read_csv(document, encoding="utf-8")
            except UnicodeDecodeError:
                return {"message": "{} could not be decoded into utf-8".format(
                    document.name)}, 422
            logging.debug("/dashboard/upload: df={}".format(df))
            df = df.select_dtypes(["number"])
            # # for now only taking first 3 rows
            df = df.iloc[:3, :]
            logging.debug("/dashboard/upload: number df={}".format(df))
            # creating neccessary objects and settings for encryption
            df = df.to_numpy()
            params = {
                "scheme": 2,
                "poly_modulus_degree": 8192*2,
                "coefficient_modulus": [60, 40, 40, 40, 60],
                "scale": pow(2.0, 40),
                "cache": True,
            }
            r = Reseal(**params)
            r.ciphertext = [0]
            # iterating through df to encrypt and store
            encrypted_list = None
            for i in df:
                logging.debug("/dashboard/upload: row numpy={}".format(i))
                i_r = r.duplicate()
                i_r.ciphertext = i
                i_r = i_r.__getstate__()
                if encrypted_list == None:
                    encrypted_list = [i_r]
                else:
                    encrypted_list.append(i_r)
                db = Mongo(arg_handler())
                db.connect()
                document = {
                    "data": ReScheme().dumps(i_r),
                    "set": set,
                    "owner": bson.objectid.ObjectId(usr.get_id()),
                    "datetime": datetime.datetime.utcnow()
                }
                id = db.dump(db_collection_name="data", data=document)
        return flask.redirect(flask.url_for(".dashboard"))

    @web_bp.route("/dashboard/infer", methods=["POST"])
    @flask_login.login_required
    def dashboard_infer():
        logging.debug("SESSION={}, user={}".format(
            flask.session, flask_login.current_user.get_id()))
        usr = flask_login.current_user
        set = flask.request.form.get("set")
        set = set if set is not "" else None
        model = flask.request.form.get("model")
        model = model if model is not "" else None
        if set is not None:
            logging.debug("/dashboard/infer: set:{}".format(set))
            pipeline = [
                {"$match": {"owner": bson.objectid.ObjectId(usr.get_id())}},
                {"$match": {"set": set}},
                {"$unset": ["data", "owner"]},
            ]
            db = Mongo(arg_handler())
            db.connect()
            cursor = db.getCursor(db_pipeline=pipeline, db_collection_name="data")
            batches = list(db.getBatches(db_data_cursor=cursor))
            if len(batches) <= 0:
                return {"message": "No data could be found that is owned by: '{}'', and has a set matching: '{}'".format(usr.name, set)}, 404
            return flask.redirect(flask.url_for(".dashboard"))
        else:
            return {"message": "No dataset to infer on was defined"}, 400

    @web_bp.route("/dashboard/view")
    @flask_login.login_required
    def dashboard_data():
        usr = flask_login.current_user
        limit = flask.request.form.get("limit")
        limit = limit if limit is not None else 50
        # get users data sorted in reverse id order (normally chronology)
        # and limit this to 50 entries for expediency.
        pipeline = [
            {"$match": {"owner": bson.objectid.ObjectId(usr.get_id())}},
            {"$unset": ["data", "owner"]},
            {"$sort": {"_id": -1}},
            {"$limit": 50},
        ]
        db = Mongo(arg_handler())
        db.connect()
        cursor = db.getCursor(db_pipeline=pipeline, db_collection_name="data")
        data_generator = db.getBatches(db_data_cursor=cursor)
        logging.debug("DASHBOARD DATA {} {}".format(data_generator,
                                                    type(data_generator)))
        return flask.render_template("data_view.jinja",
                                     data_generator=data_generator)

    @web_bp.route("/dashboard/view/<id>")
    @flask_login.login_required
    def dashboard_view():
        return flask.render_template("data_view.jinja", data_generator=None)

    # returning now fully instantiated object
    return web_bp


def build_less_assets(assets: dict):
    for key in assets:
        origin = os.path.abspath(key)
        destination = os.path.abspath(assets[key])
        with open(origin, "r") as f:
            compiled = lesscpy.compile(f, minify=True)
        # logging.info("CSS: {}->{}\n{}".format(origin, destination, compiled))
        try:
            os.mkdir(os.path.split(destination)[0])
        except FileExistsError:
            pass
        with open(destination, "w") as f:
            f.write(compiled)


def login_management(bp):
    from api.user import User
    from api.api import arg_handler
    login_manager = flask_login.LoginManager()
    # what page to redirect to for login
    login_manager.login_view = "web.login"
    # cookie setting
    login_manager.session_protection = "strong"
    login_manager.REMEMBER_COOKIE_SECURE = True
    login_manager.REMEMBER_COOKIE_HTTPONLY = True

    db = Mongo(arg_handler())

    @login_manager.user_loader
    def load_user(user_id):
        """Either return user object, or None, if found/ not respectiveley."""
        usr = User(id=user_id)
        # if user doc is not None I.E user exists in db
        if usr.doc:
            result = usr
        else:
            result = None
        logging.debug(
            "LOGIN_MANAGER: user_loader; user_id={}, return={}".format(
                user_id, result))
        return result

    @login_manager.request_loader
    def request_loader(request):
        username = flask.request.form.get("username")
        password = flask.request.form.get("password")
        result = None
        if password and username:
            usr = User(username=username, password=password)
            if usr.doc:
                result = usr
        logging.debug(
            "LOGIN_MANAGER: request_loader; username={}, return={}".format(
                username, result))
        return result

    return login_manager


if __name__ == "__main__":
    app = flask.Flask(__name__)
    # generating API object
    web = web_main()
    # registering blueprint so we can test it if this file is run alone
    app.config["SERVER_NAME"] = "cryptolog.io"
    # app.config["SERVER_NAME"] = "localhost:8000"
    app.register_blueprint(web, subdomain="web")
    # print(app.config)
    # print(app.url_map)
    app.run(
        host="localhost",
        port=443,
        debug=True,
        ssl_context="adhoc"  # gaff https
    )
