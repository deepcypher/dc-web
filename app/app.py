#!/usr/bin/env python3

# @Author: GeorgeRaven <archer>
# @Date:   2020-06-17T15:30:36+01:00
# @Last modified by:   archer
# @Last modified time: 2021-01-15T15:20:01+00:00
# @License: please see LICENSE file in project root

import os
import sys
import werkzeug
import flask
import flask_assets
import logging
from ezdb.mongo import Mongo
from api.api import arg_handler
from api.user import User


def bundle_assets(app):
    assets = flask_assets.Environment(app)

    default_less_bundle = flask_assets.Bundle(
        "src/less/default.less",
        filters="less",  # "less,cssmin"
        output="css/default_bundle.css",
        extra={"rel": "stylesheet/css"}
    )
    assets.register("default_styles", default_less_bundle)
    default_less_bundle.build()

    return assets


def main():
    app = flask.Flask(__name__, template_folder=None, static_folder=None)
    app.secret_key = "SetThisToS0methingVeryLong4ndKeepItAGoodSecretWhileInUse"
    # os.urandom(256)
    return app


def entry_point():
    from api.api import api_main
    from web.web import web_main
    # from phd.web import phd_main
    app = main()
    # generating API object
    api = api_main()
    web = web_main()
    # registering blueprint so we can test it if this file is run alone
    app.config["SERVER_NAME"] = "deepcypher.me"
    # app.config["SERVER_NAME"] = "localhost:5001"
    app.register_blueprint(web)
    app.register_blueprint(api, subdomain="api")
    # print(app.config)
    logging.info("Launching flask webserver...")
    logging.info("Routes: \n{}".format(app.url_map))
    return app


if __name__ == "__main__":
    logging.basicConfig(filename="./flask.log", level=logging.DEBUG,
                        format="%(asctime)s %(levelname)s:%(message)s",
                        datefmt="%Y-%m-%dT%H:%M:%S")
    app = entry_point()
    # changing to dev address cryptologdev.io in /etc/hosts should be localhost
    app.run(
        host="localhost",
        port=443,
        debug=True,
        ssl_context="adhoc"  # gaff https
    )
else:
    pass
    # logging.basicConfig(filename="/flask/flask.log", level=logging.DEBUG,
    #                     format="%(asctime)s %(levelname)s:%(message)s",
    #                     datefmt="%Y-%m-%dT%H:%M:%S")
