FROM archlinux:latest

ARG CONTAINER_APP_DIR="/app"
ARG CONTAINER_APP="app.py"

# resetting all gnupg keys
RUN rm -rf /etc/pacman.d/gnupg && \
    pacman-key --init && \
    pacman-key --populate archlinux

# Install dependencies and retrieve seal-python files
RUN pacman -Syyuu \
    archlinux-keyring \
    base-devel \
    --noconfirm

RUN pacman -S cmake \
    clang \
    git \
    python \
    python-pip \
    python-sphinx \
    python-sphinx-argparse \
    python-sphinx_rtd_theme \
    python-configargparse \
    --noconfirm

RUN git clone -b 3.4.5-rlatest https://github.com/DreamingRaven/seal-python

# Build SEAL packages in seal-python
RUN cd /seal-python/SEAL/native/src && \
    cmake . && \
    make && \
    make install && \
    echo "/usr/local/lib" >> /etc/ld.so.conf.d/seal.conf && \
    ldconfig

# Install requirements of seal-python
RUN cd /seal-python && \
    pip3 install -r requirements.txt

# Build pybind11
RUN cd /seal-python/pybind11 && \
    mkdir build && \
    cd /seal-python/pybind11/build && \
    cmake .. && \
    make check -j 4 && \
    make install

# Package wrapper
RUN cd /seal-python && \
    python3 setup.py build_ext -i && \
    python3 setup.py install

# copy in python requirements and install
COPY "./app/requirements.txt" "${CONTAINER_APP_DIR}/requirements.txt"
RUN pip3 install -r "${CONTAINER_APP_DIR}/requirements.txt"

# copy in everything else
COPY "./app/" "${CONTAINER_APP_DIR}"
COPY "./LICENSE" "${CONTAINER_APP_DIR}/LICENSE"

# debug and check everything is there as expected
RUN cd "${CONTAINER_APP_DIR}" && \
    ls -al --color && \
    echo "Hello, test-container here. Please check above ls show the files you expect in your app."

# Clean-up non-needed crap and any last second touch ups
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    mkdir /flask

# this will probably fail but it will error in such a way as to let people know how to use it properly
# FYI it needs environment variables to let it know the db credentials etc
CMD gunicorn --workers=2 --chdir /app --bind 0.0.0.0:80 --log-level debug --access-logformat '%(h)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s" "%(D)s' --keep-alive 10 "app:entry_point()"

# REMEMBER YOU MUST PASS IN DB USER NAME AND DB PASSWORD either through config or env
